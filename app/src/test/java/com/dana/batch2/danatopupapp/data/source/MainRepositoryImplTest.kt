package com.dana.batch2.danatopupapp.data.source

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.RemoteDataSource
import com.dana.batch2.danatopupapp.data.source.remote.request.LoginRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.SendOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.UploadTransferRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.VerifyOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.*
import com.dana.batch2.danatopupapp.utils.LiveDataUtilsTest
import com.dana.batch2.danatopupapp.utils.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.io.File

class MainRepositoryImplTest {

    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var remoteDataSource: RemoteDataSource



    private lateinit var mainRepository: MainRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mainRepository = MainRepositoryImpl(remoteDataSource)
    }

    @Test
    fun postLogin() {
        val loginRequest = LoginRequest("6289653399715", "Qwerty00@@")
        val loginResponse = provideLoginResponse()
        val loginResponseLive = MutableLiveData<ApiResponse<LoginResponse>>()
        loginResponseLive.value = ApiResponse.success(loginResponse)

        Mockito.`when`(mainRepository.postLogin(loginRequest)).thenReturn(loginResponseLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.postLogin(loginRequest))

        Mockito.verify(remoteDataSource).postLogin(loginRequest)

        assertNotNull(result.body)
        assertEquals(loginResponse, result.body)
    }

    @Test
    fun postSignUp() {
        val signUpRequest = provideSignUpRequest()
        val signUpResponse = NonBodyResponse("success", 200)
        val signUpResponseLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        signUpResponseLive.value = ApiResponse.success(signUpResponse)

        Mockito.`when`(remoteDataSource.postSignUp(signUpRequest)).thenReturn(signUpResponseLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.postSignUp(signUpRequest))

        Mockito.verify(remoteDataSource).postSignUp(signUpRequest)

        assertNotNull(result.body)
        assertEquals(signUpResponse, result.body)
    }

    @Test
    fun postSendOtp() {
        val sendOtpRequest = SendOtpRequest("6289653399715", "rizkyalfikri@gmail.com")
        val sendOtpResponse = NonBodyResponse("success", 200)
        val sendOtpResponseLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        sendOtpResponseLive.value = ApiResponse.success(sendOtpResponse)

        Mockito.`when`(remoteDataSource.postSendOtp(sendOtpRequest)).thenReturn(sendOtpResponseLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.postSendOtp(sendOtpRequest))

        Mockito.verify(remoteDataSource).postSendOtp(sendOtpRequest)

        assertNotNull(result.body)
        assertEquals(sendOtpResponse, result)
    }

    @Test
    fun postVerifyOtp() {
        val verifyOtpRequest = VerifyOtpRequest("6289653399715", 123456)
        val verifyOtpResponse = NonBodyResponse("success", 200)
        val verifyOtpResponseLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        verifyOtpResponseLive.value = ApiResponse.success(verifyOtpResponse)

        Mockito.`when`(remoteDataSource.postVerifyOtp(verifyOtpRequest)).thenReturn(verifyOtpResponseLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.postVerifyOtp(verifyOtpRequest))

        Mockito.verify(remoteDataSource).postVerifyOtp(verifyOtpRequest)

        assertNotNull(result.body)
        assertEquals(verifyOtpResponse, result.body)
    }

    @Test
    fun getUserProfile() {
        val userProfile = getUserRequestTest()
        val userProfileLive = MutableLiveData<ApiResponse<UserProfileResponse>>()
        userProfileLive.value = ApiResponse.success(userProfile)

        Mockito.`when`(remoteDataSource.getUserProfile(authToken)).thenReturn(userProfileLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.getUserProfile(authToken))

        Mockito.verify(remoteDataSource).getUserProfile(authToken)

        assertNotNull(result.body)
        assertEquals(userProfile, result.body)
    }

    @Test
    fun getUserBalance() {
        val userBalance = UserBalanceResponse(3720000F)
        val userBalanceLive = MutableLiveData<ApiResponse<UserBalanceResponse>>()
        userBalanceLive.value = ApiResponse.success(userBalance)

        Mockito.`when`(remoteDataSource.getUserBalance(authToken)).thenReturn(userBalanceLive)

        val result = LiveDataUtilsTest.getValue(mainRepository.getUserBalance(authToken))

        Mockito.verify(remoteDataSource).getUserBalance(authToken)

        assertNotNull(result.body)
        assertEquals(userBalance, result.body)
    }

    @Test
    fun getCatalogBalance() {

        val balanceCatalogs = provideBalanceCatalogTest()
        val balanceCatalogLive = MutableLiveData<ApiResponse<List<BalanceCatalogResponse>>>()
        balanceCatalogLive.value = ApiResponse.success(balanceCatalogs)

        Mockito.`when`(remoteDataSource.getCatalogBalance(authToken)).thenReturn(balanceCatalogLive)

        val result = LiveDataUtilsTest.getValue(mainRepository.getCatalogBalance(authToken))

        Mockito.verify(remoteDataSource).getCatalogBalance(authToken)

        assertNotNull(result.body)
        assertEquals(balanceCatalogs, result.body)

    }

    @Test
    fun getPaymentMethod() {
        val paymentTypes = listOf(
            PaymentMethodResponse(1, 1, "BCA"),
            PaymentMethodResponse(2, 2, "Indomaret")
        )

        val paymentTypesLive = MutableLiveData<ApiResponse<List<PaymentMethodResponse>>>()
        paymentTypesLive.value = ApiResponse.success(paymentTypes)

        Mockito.`when`(remoteDataSource.getPaymentMethod(authToken)).thenReturn(paymentTypesLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.getPaymentMethod(authToken))

        Mockito.verify(remoteDataSource).getPaymentMethod(authToken)

        assertNotNull(result.body)
        assertEquals(paymentTypes, result.body)
    }

    @Test
    fun getTopUpHistory() {
        val topUpHistory = provideTopUpHistory()
        val topUpHistoryLive = MutableLiveData<ApiResponse<List<TopUpHistoryResponse>>>()
        topUpHistoryLive.value = ApiResponse.success(topUpHistory)

        Mockito.`when`(remoteDataSource.getTopUpHistory(authToken)).thenReturn(topUpHistoryLive)

        val result =
            LiveDataUtilsTest.getValue(mainRepository.getTopUpHistory(authToken))

        Mockito.verify(remoteDataSource).getTopUpHistory(authToken)

        assertNotNull(result.body)
        assertEquals(topUpHistory, result.body)
    }

    @Test
    fun postTopUpBalance() {
        val topUpBalance = provideTopUpBalance()
        val topUpBalanceLive = MutableLiveData<ApiResponse<TopUpBalanceResponse>>()
        topUpBalanceLive.value = ApiResponse.success(topUpBalance)

        Mockito.`when`(remoteDataSource.postTopUpBalance(authToken, provideTopUpRequest()))
            .thenReturn(topUpBalanceLive)

        val result = LiveDataUtilsTest.getValue(
            mainRepository.postTopUpBalance(
                authToken,
                provideTopUpRequest()
            )
        )

        Mockito.verify(remoteDataSource).postTopUpBalance(authToken, provideTopUpRequest())

        assertNotNull(result.body)
        assertEquals(topUpBalance, result.body)
    }

    @Test
    fun postTransferReceipt() {
        val uploadTransfer = UploadTransferRequest(File("Path"))
        val nonBodyResponse = NonBodyResponse("success", 200)
        val paymentToken = "80006289653399715"
        val uploadTransferLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        uploadTransferLive.value = ApiResponse.success(nonBodyResponse)

        Mockito.`when`(remoteDataSource.postTransferReceipt(authToken, uploadTransfer, paymentToken))
            .thenReturn(uploadTransferLive)

        val result = LiveDataUtilsTest.getValue(
            mainRepository.postTransferReceipt(
                authToken,
                uploadTransfer,
                paymentToken
            )
        )

        Mockito.verify(remoteDataSource)
            .postTransferReceipt(authToken, uploadTransfer, paymentToken)

        assertNotNull(result.body)
        assertEquals(nonBodyResponse, result.body)
    }

}