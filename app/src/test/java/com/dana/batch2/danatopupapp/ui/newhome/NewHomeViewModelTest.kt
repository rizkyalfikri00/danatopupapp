package com.dana.batch2.danatopupapp.ui.newhome

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserBalanceResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NewHomeViewModelTest {

    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerHomeFragment: Observer<ApiResponse<UserBalanceResponse>>

    private lateinit var newHomeViewModel: NewHomeViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@NewHomeViewModelTest)
        newHomeViewModel = NewHomeViewModel(mainRepository)

    }

    @Test
    fun getUserBalance() {
        val userBalance = ApiResponse.success(UserBalanceResponse(3720000F))
        val userBalanceLive = MutableLiveData<ApiResponse<UserBalanceResponse>>()
        userBalanceLive.value = userBalance

        Mockito.`when`(mainRepository.getUserBalance(authToken)).thenReturn(userBalanceLive)

        newHomeViewModel.setAuthToken(authToken)
        newHomeViewModel.getUserBalance().observeForever(observerHomeFragment)

        verify(observerHomeFragment).onChanged(userBalance)
    }
}