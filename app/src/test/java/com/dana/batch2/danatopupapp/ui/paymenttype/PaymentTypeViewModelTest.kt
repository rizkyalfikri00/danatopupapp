package com.dana.batch2.danatopupapp.ui.paymenttype

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpBalanceResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.dana.batch2.danatopupapp.utils.provideTopUpBalance
import com.dana.batch2.danatopupapp.utils.provideTopUpRequest
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PaymentTypeViewModelTest {


    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerPaymentType: Observer<ApiResponse<List<PaymentMethodResponse>>>

    @Mock
    private lateinit var observerTopUpBalance: Observer<ApiResponse<TopUpBalanceResponse>>

    private lateinit var paymentTypeViewModel: PaymentTypeViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@PaymentTypeViewModelTest)
        paymentTypeViewModel = PaymentTypeViewModel(mainRepository)
    }

    @Test
    fun getPaymentMethod() {
        val paymentTypes = ApiResponse.success(
            listOf(
                PaymentMethodResponse(1, 1, "BCA"),
                PaymentMethodResponse(2, 2, "Indomaret")
            )
        )

        val paymentTypesLive = MutableLiveData<ApiResponse<List<PaymentMethodResponse>>>()
        paymentTypesLive.value = paymentTypes

        Mockito.`when`(mainRepository.getPaymentMethod(authToken)).thenReturn(paymentTypesLive)

        paymentTypeViewModel.setAuthToken(authToken)
        paymentTypeViewModel.getPaymentMethod().observeForever(observerPaymentType)

        verify(observerPaymentType).onChanged(paymentTypes)
    }

    @Test
    fun postTopUpBalance() {
        val topUpBalance = ApiResponse.success(provideTopUpBalance())
        val topUpBalanceLive = MutableLiveData<ApiResponse<TopUpBalanceResponse>>()
        topUpBalanceLive.value = topUpBalance

        Mockito.`when`(mainRepository.postTopUpBalance(authToken, provideTopUpRequest()))
            .thenReturn(topUpBalanceLive)

        paymentTypeViewModel.setAuthToken(authToken)
        paymentTypeViewModel.postTopUpBalance(provideTopUpRequest()).observeForever(observerTopUpBalance)

        verify(observerTopUpBalance).onChanged(topUpBalance)
    }
}