package com.dana.batch2.danatopupapp.application

import android.app.Application
import com.dana.batch2.danatopupapp.di.AppComponent
import com.dana.batch2.danatopupapp.di.AppModule
import com.dana.batch2.danatopupapp.di.DaggerAppComponent

class DanaTopUpApplication : Application() {

    lateinit var dataTopUpApplication: AppComponent

    private fun initDagger(app: DanaTopUpApplication): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(app))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        dataTopUpApplication = initDagger(this@DanaTopUpApplication)
    }

}