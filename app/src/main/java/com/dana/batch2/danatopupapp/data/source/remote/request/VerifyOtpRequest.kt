package com.dana.batch2.danatopupapp.data.source.remote.request

import com.google.gson.annotations.SerializedName

data class VerifyOtpRequest (
    @SerializedName("phone_number")
    var phoneNumber: String,

    @SerializedName("otp_code")
    var otp_code: Int
)