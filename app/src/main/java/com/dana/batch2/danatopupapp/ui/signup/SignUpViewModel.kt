package com.dana.batch2.danatopupapp.ui.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.SignUpRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.NonBodyResponse

class SignUpViewModel(private val mainRepository: MainRepository): ViewModel() {

    fun processSignUp(signUpRequest: SignUpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        return mainRepository.postSignUp(signUpRequest)
    }
}