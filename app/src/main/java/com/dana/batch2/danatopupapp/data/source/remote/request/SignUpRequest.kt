package com.dana.batch2.danatopupapp.data.source.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SignUpRequest (
    @SerializedName("email")
    var email: String,
    @SerializedName("first_name")
    var firstName: String,
    @SerializedName("last_name")
    var lastName: String,
    @SerializedName("password")
    var password: String,
    @SerializedName("phone_number")
    var phoneNumber: String
) : Parcelable