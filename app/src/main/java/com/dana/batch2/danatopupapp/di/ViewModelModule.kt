package com.dana.batch2.danatopupapp.di

import androidx.lifecycle.ViewModelProvider
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule {

    @Provides
    @Singleton
    fun provideViewModelFactory(mainRepository: MainRepository): ViewModelProvider.NewInstanceFactory {
        return ViewModelFactory(mainRepository)
    }
}