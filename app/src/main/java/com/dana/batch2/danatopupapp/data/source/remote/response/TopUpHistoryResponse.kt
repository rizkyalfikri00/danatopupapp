package com.dana.batch2.danatopupapp.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TopUpHistoryResponse(

    @SerializedName("id")
    var invoiceId: Int,

    @SerializedName("user_id")
    var userId: Int,

    @SerializedName("topup_balance")
    var topUpBalance: Float,

    @SerializedName("payment_type")
    var paymentType: Int,

    @SerializedName("name")
    var paymentName: String,

    @SerializedName("token")
    var paymentToken: String,

    @SerializedName("status")
    var status: Int,

    @SerializedName("created_at")
    var createdAt: String,

    @SerializedName("path")
    var filePath: String?

) : Parcelable



