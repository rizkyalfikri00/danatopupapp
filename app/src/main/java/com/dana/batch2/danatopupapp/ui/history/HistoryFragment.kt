package com.dana.batch2.danatopupapp.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dana.batch2.danatopupapp.R
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        init()
    }

    private fun init() {
        val sectionsPagerAdapter =
            SectionsPagerAdapter(
                requireActivity(),
                childFragmentManager
            )

        viewPager.adapter = sectionsPagerAdapter
        tabHistory.setupWithViewPager(viewPager)
    }
}
